/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.leonardmreidy.blackout.geo.tools.Distance;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * TODO: JavaDoc for DBHelper class
 */
public class DBHelper extends SQLiteAssetHelper {


    private static final String DATABASE_NAME = "busStopInfoDB4AndroidSQLite";
    private static final int DATABASE_VERSION = 1;

    private static String DATABASE_TABLE = "busStopInfoDB";
    private static String TAG = "DBHelper";

    // Fields of interest in busStopInfoDB4AndroidSQLite
    public static final String KEY_ID = "_id";
    public static final String STOP_ID = "stopId";
    public static final String FULL_NAME = "fullname";
    public static final String OPERATOR_NAME = "operatorName";
    public static final String ROUTE = "route";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";


    /**
     * Create a helper object to create, open, and/or manage a database in
     * the application's default private data directory.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use to open or create the database
     * @param name    of the database file
     * @param factory to use for creating cursor objects, or null for the default
     * @param version number of the database (starting at 1); if the database is older,
     *                SQL file(s) contained within the application assets folder will be used to
     */
    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    /**
     * Querying a Database
     * Each database query is returned as a Cursor. This lets Android manage resources more efficiently
     * by retrieving and releasing row and column values on demand. To execute a query on a db object,
     * use the query method, passing in the following:
     *
     *      # [optional] a Boolean that specifies whether the result set should contain only unique values;
     *      # the name of the table;
     *      # a projection (as an array of Strings), that lists the columns to be included in the result set;
     *      # a 'where' clause that defines the rows to be returned. You can include (?) wildcards that will be
     *      replaced by the values passed in via the selection argument parameter;
     *      # an array of selection argument Strings to replace the (?) wildcards in the where clause;
     *      # a 'group by' clause that determines how the resulting rows will be grouped;
     *      # a 'having; clause that determines which row groups to include in the event that the 'group by'
     *      clause has been specified;
     *      # a String that specifies the order of the returned rows;
     *      # a String that specifies the maximum number of rows in the result set.
     *
     */
    public Cursor getRecord(String fullNameContains) throws SQLException
    {
        /**
         * Specify results column projection.
         * Return minimum set of columns required to satisfy requirements.
         * I may simply overload this method for a set of simple common queries.
         * For example, I can get the operator and the routes for a given stopId
         * without querying the dublinked server again. This may be useful in other
         * parts of the application.
         */
        String[]  result_columns = new String[]{
                FULL_NAME, STOP_ID, LATITUDE, LONGITUDE
        };

        // Specify the where clause that will limit our results
        String where = FULL_NAME + " LIKE " + "'"+fullNameContains+"'";

        /** Replace these with valid SQL statements as necessary **/
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = getReadableDatabase();

        // Execute query and get corresponding cursor
        Cursor cursor = db.query(DBHelper.DATABASE_TABLE, result_columns, where, whereArgs, groupBy, having, order);

        return cursor;
    }

    /**
     * Given the coordinates provided by Google Place/Nearby search for nearest transit station,
     * get the Dublinked stopId from the local copy of the Bus Stop Information data store.
     *
     * @param cursor
     * @return
     */
    public ArrayList<HashMap<String, String>> getQueryResultsFromCursor(Cursor cursor)
    {
        String result = "";
        Integer count = new Integer(0);

        ArrayList<HashMap<String, String>> resultSet = new ArrayList<>();

        // Find the index to the column(s) being used.
        int FULL_NAME_INDEX = cursor.getColumnIndexOrThrow(FULL_NAME);
        int STOP_ID_INDEX = cursor.getColumnIndexOrThrow(STOP_ID);
        int LATITUDE_INDEX = cursor.getColumnIndexOrThrow(LATITUDE);
        int LONGITUDE_INDEX = cursor.getColumnIndexOrThrow(LONGITUDE);

        while (cursor.moveToNext())
        {
            HashMap<String, String> testMap = new HashMap<String, String>();

            testMap.put("fullname", cursor.getString(FULL_NAME_INDEX));
            testMap.put("stopId", cursor.getString(STOP_ID_INDEX));
            testMap.put("latitude", cursor.getString(LATITUDE_INDEX));
            testMap.put("longitude", cursor.getString(LONGITUDE_INDEX));

            resultSet.add(testMap);
        }

        return resultSet;
    }
    public String getMostLikelyStopIdFromQueryResults(ArrayList<HashMap<String, String>> recordStrs, String googleCoords)
    {

        String result = "Result not available...";

        // Create an array to hold the coordinates given by Google for the nearest transit station
        String[] googleCoordsArr = new String[2];

        // Take the Google coordinate string and split on the comma to extract the latitude and longitude
        googleCoordsArr = googleCoords.split(",");

        // Convert the String representation of latitude and longitude to doubles
        double googleLat = Double.parseDouble(googleCoordsArr[0]);
        double googleLon = Double.parseDouble(googleCoordsArr[1]);

        // Create data structure (TreeMap) to store the query results of interest ordered by great circle distance
        SortedMap<Double, String> resultsByGreatCircleDistance = new TreeMap<>();

        /** Iterate over the given ArrayList of Hashmaps of Cursor records, calculate the great circle distance,
         *  between the Google coordinates and the given record's coordinates, then append to the treemap
         *  which will sort them by the natural ordering of their keys, i.e., shortest great circle distance first!
         */
        for(HashMap<String, String> record : recordStrs)
        {
            double greatCircleDistance = Distance.HaverSineDistance(googleLat, googleLon,
                    Double.parseDouble(record.get("latitude")), Double.parseDouble(record.get("longitude")));
            resultsByGreatCircleDistance.put(greatCircleDistance, record.get("stopId"));
        }

        /**
         * Return the most likely stop id by getting the value of the first key-value pair in the
         * resultsByGreatCircleDistance TreeMap
         */
        try
        {
            result = resultsByGreatCircleDistance.get(resultsByGreatCircleDistance.firstKey());
        }
        catch(NoSuchElementException nsee)
        {
            //TODO: get a list of SQLExceptions and add a catch() for each!
            Log.d(TAG, "NoSuchElementException");
        }
        catch(Exception e)
        {
            Log.d(TAG, e.getMessage());
        }

        finally
        {
            // TODO: determine a role for finally, or eliminate it

        }
        return result;

    }
}


