/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */

package com.leonardmreidy.blackout;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.leonardmreidy.blackout.dialogs.FlightModeSettingsWarningDialogFragment;

/**
 * This class manages the splash screen and elementary application initialisation
 * for the Blackout Android application.
 *
 * The application exploits the Dublinked Real-Time Passenger Information REST service, Google Maps
 * web services and related APIs to provide the user with real-time information about nearby transit
 * services. The lion's share of the UI/UX is managed by a customised Google Map (in MapsActivity).
 * In order to provide a fluid user experience, the application must acquire a one-off location fix
 * during initialisation and pass the resulting coordinates to the map object in the MapsActivity.
 * The MapsActivity uses the given coordinates to load the map at the user's current location,
 * before executing a series of asynchronous web service calls to acquire the relevant information
 * about nearby transit services and locations. Once the information is acquired, the map updates
 * with a cluster of annotated markers at all relevant locations within n kilometers(TBD) of the
 * user's current location.
 *
 * Ideally, the application should acquire the initial location fix using the platform location
 * API, GPS and/or Android's Network Location Provider. GPS only works outdoors. But Android's
 * Network Location Provider determines the user's location using cell tower and Wi-Fi signals,
 * more quickly and more accurately than GPS. Moreover, it works indoors. Given the objectives
 * for the splash screen activity, the best strategy devolves the responsibility of identifying
 * the most suitable provider to the OS (using Criteria).
 */
public class Splash extends Activity {

    private final String TAG = "## DEBUG/SPLASH: ##";      // Debug tag
    private static int SPLASH_TIME_OUT = 5000;             // Splash screen timer default
    private Location currentLocation;                      // Current device location
    String currentLocationCoords;                          // Current device location coordinates


    /** Getters and Setters **/


    /**
     * <p>Standard initialisation sequence inherited from base class plus:
     * set current location of the device with the best results the Location Manager can provide
     * with the given criteria, and pass the current location coordinates to the MapsActivity
     * as a starting location.</p>
     * @param savedInstanceState saved state of object
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.i(TAG, " onCreate() executing...");


        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        /**
         * Try to set the current location with the best results the Location Manager can provide,
         * with the given criteria. If the attempt is successful, reduce the splash screen timeout
         * to ensure a fluid user experience. If a security exception is raised, and it almost
         * certainly will be if the device runs Android 6 (API level >= 23), perform permission
         * check (TODO: implement permission check).
         */
        try
        {
            setCurrentLocation(getBestLocationFromLocationManager());
            setSplashTimeOut(2000);
        }
        catch(SecurityException se)
        {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED)
            {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        // Parse the given Location object for a String containing latitude and longitude
        currentLocationCoords = updateWithNewLocation(currentLocation);

        /** Keep the Splash screen visible for at least two seconds, in the event
         * that the current location has been successfully updated. If not,
         * keep it visible for 5 seconds.
         */
        new Handler().postDelayed(new Runnable()
        {

            /** Showing splash screen with a timer. This is useful for showcasing the app logo,
             * and masking some of the initialisation activities.
             */
            @Override
            public void run()
            {

                Log.i(TAG, " postDelayed() handler executing...");


                    // Package up coordinates in an Intent and start the main activity
                    Intent splashIntent = new Intent(Splash.this, MapsActivity.class);

                    splashIntent.putExtra("startingCoordinates",currentLocationCoords);

                    startActivity(splashIntent);

                    // close this activity
                    finish();


            }
        }, SPLASH_TIME_OUT);
    }

    /**
     * Set splash screen timeout duration. The default is 5 seconds.
     * @param splashTimeout
     */
    private void setSplashTimeOut(int splashTimeout)
    {
        Log.i(TAG, " setSplashTimeOut() executing...");
        SPLASH_TIME_OUT = splashTimeout;
    }

    /**
     * Get splash screen timeout duration; the default is 5 seconds.
     * @return current splash screen timeout duration
     */
    public int getSplashTimeOut()
    {
        return SPLASH_TIME_OUT;
    }

    /**
     * Set the currentLocation member variable.
     * @param currentLocation the current location of the device
     */
    public void setCurrentLocation(Location currentLocation)
    {
        Log.i(TAG, " setCurrentLocation() executing...");

        this.currentLocation = currentLocation;
    }

    /**
     * Get the current location (Location object).
     * @return
     */
    public Location getCurrentLocation()
    {
        return currentLocation;
    }

    /**
     * <p>Returns the current location as a String containing latitude and longitude. If the Location
     * passed to the method is not null, the latitude and longitude properties of the Location
     * object are retrieved and appended to an empty String. The resulting coordinate String
     * (currentLocationCoords) will be packaged up in an Intent Bundle to be passed to the
     * MapsActivity where it will be parsed and used by the Google Maps object to determine a
     * sensible starting location.</p>
     * @param location the current location retrieved from LocationManager
     * @return coordinates as a String (latitude and longitude separated by a comma for easy parsing)
     */
    private String updateWithNewLocation(Location location)
    {
        Log.i(TAG, " updateNewLocation() executing...");

        String latLngString = "No Location Found";
        if (location != null)
        {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            latLngString = ""+lat+","+lng+"";
        }
        return latLngString;
    }

    /**
     * Get the best location from the location manager, given a set of criteria that
     * include accuracy, power requirements, and cost.
     * <p>
     * Although it is important to determine the location of the device it is not necessary to
     * achieve a very high degree of accuracy at the time the map in MapsActivity is first loaded.
     * It is only when the application begins making requests to the various web services it relies
     * on to collect real-time transit/location information that accuracy becomes a factor. As such,
     * a fairly limited set of criteria constrain how the best location is determined.
     * </p>
     * <p> The method proceeds as follows:
     * <ol>
     * <li> Get a reference to a <code>LocationManager</code>.
     * </li>
     * <li>Get the name of the relevant service from the <code>Context</code>, an interface to
     * global information about the application environment.(<code>Context.LOCATION_SERVICE</code>
     * identifies the service of interest; used when retrieving a <code>LocationManager</code>
     * for managing location updates).
     * </li>
     * <li>Get a handle to the system-level service (location service of interest) and cast it to
     * type <code>LocationManager</code>.
     * </li>
     * <li>Instantiate a new <code>Criteria</code> object. The <code>Criteria</code> class indicates
     * the application criteria for selecting a location provider, where providers are ordered
     * according to accuracy, power usage, ability to report altitude, speed, bearing and monetary cost.
     * </li>
     * <li>Set criteria. <code>Criteria</code> for BlackOut app initialisation are minimal.</li>
     * <li>Given the criteria, get the best provider from the <code>LocationManager</code>.</li>
     * <li>Define a <code>Looper</code>, set to <code>null</code>, to ensure only a single location
     * update is requested.</li>
     * <li>Try to get a single location update. Report exceptions if the attempt fails.</li>
     * <li>Get the last known location, in the event that the single location update failed.</li>
     * </ol>
     * </p>
     * @return  a <code>Location</code> object representing the <code>LocationManager's</code> best
     * estimate of current device location
     * @throws SecurityException
     */
    private Location getBestLocationFromLocationManager() throws SecurityException
    {
        Log.i(TAG, " getBestLocationFromLocationManager() executing...");

        // Get a reference to a location manager
        LocationManager locationManager;

        // Identify the location service
        String svcName = Context.LOCATION_SERVICE;

        // Get a handle to the system-level location service
        locationManager = (LocationManager) getSystemService(svcName);

        // Set a minimal collection of criteria
        Criteria criteria = new Criteria();
        // Set accuracy to coarse to circumvent problems that arise when indoors
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);

        /** Get the best provider with the given criteria
         * getBestProvider() returns the name of the provider that best meets
         * the given criteria. If several providers meet the criteria, the one
         * with best accuracy is returned. If no provider meets the criteria,
         * they are loosened in the following sequence:
         * > power requirement
         * > accuracy
         * > bearing
         * > speed
         * > altitude
         */
        String provider = locationManager.getBestProvider(criteria, true);

        // Set up a looper for the single update request
        Looper looper = null;

        // Try to execute a single update request
        try
        {
            locationManager.requestSingleUpdate(criteria, locationListener, looper);
        }
        catch(IllegalArgumentException iae)
        {
            Log.i(TAG, iae.getMessage());
        }
        catch(SecurityException se)
        {
            // If a security exception happens here it should be handled in the onCreate() method
            Log.i(TAG, se.getMessage());
        }

        // Get the last known location, in the event that the single update failed
        Location bestLocation = locationManager.getLastKnownLocation(provider);

        return bestLocation;
    }

    /**
     * TODO: add Javadoc for locationListener
     */
    private final LocationListener locationListener = new LocationListener()
    {
        public void onLocationChanged(Location location)
        {
            Log.i(TAG, " locationListener.onLocationChanged() executing...");
            updateWithNewLocation(location);
        }

        public void onProviderDisabled(String provider)
        {
            Log.i(TAG, " locationListener.onProviderDisabled() executing...");
            // TODO: Decide whether or not implementation is required
        }
        public void onProviderEnabled(String provider)
        {
            Log.i(TAG, " locationListener.onProviderEnabled() executing...");
            // TODO: Decide whether or not implementation is required
        }
        public void onStatusChanged(String provider, int status, Bundle extras)
        { Log.i(TAG, " locationListener.onProviderEnabled() executing...");
            // TODO: Decide whether or not implementation is required

        }
    };

}
