/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.geo.tools;

/**
 * This class uses the Haversine Formula to calculate the great circle distance between two points
 * on Earth, and the Vincenty formula to calculate the geodesic for a given ellipsoidal model of
 * the earth's geometry. My Haversine implementation respects the constraints of the method for
 * calculating short distances usually credited to Caltech's Bob Chamberlain. Here is how he
 * describes it on the Geographic Information Systems FAQ page at
 *
 * http://www.faqs.org/faqs/geography/infosystems-faq/:
 *
 * "[P]resuming a spherical Earth with radius R (see below), and the locations of the
 * two points in spherical coordinates (longitude and latitude) are lon1, lat1 and
 * lon2, lat2 then the Haversine Formula
 * (from R.W. Sinnott, "Virtues of the Haversine", Sky and Telescope, vol. 68, no. 2, 1984, p. 159):
 *
 * 		dlon = lon2 - lon1
 * 		dlat = lat2 - lat1
 * 		a = sin^2(dlat/2) + cos(lat1) * cos(lat2) * sin^2(dlon/2)
 * 		c = 2 * arcsin(min(1,sqrt(a)))
 * 		d = R * c
 *
 * will give mathematically and computationally exact results. The intermediate result c is the great
 * circle distance in radians. The great circle distance d will be in the same units as R. The min()
 * function protects against possible roundoff errors that could sabotage computation of the arcsine
 * if the two points are very nearly antipodal (that is, on opposide sides of the Earth). Under these
 * conditions, the Haversine Formula is ill-conditioned (see the discussion below), but the error,
 * perhaps as large as 2 km (1 mi), is in the context of a distance near 20,000 km (12,000 mi).
 *
 * Most computers require the arguments of trignometric functions to be expressed in radians. To convert
 * lon1, lat1 and lon2,lat2 from degrees, minutes, and seconds to radians, first convert them to decimal
 * degrees. To convert decimal degrees to radians, multiply the number of degrees by pi/180 = 0.017453293
 * radians/degree. [In my implementation, the coordinates are provided by the Google and Dublinked services
 * in decimal degrees. As such, I can skip the first conversion.]
 *
 * Inverse trigonometric functions return results expressed in radians. To express c in decimal degrees,
 * multiply the number of radians by 180/pi = 57.295780 degrees/radian. (But be sure to multiply the
 * number of RADIANS by R to get d.)" (See above for source)
 *
 *
 */
public class Distance
{
    static double lat1;
    static double lat2;
    static double lon1;
    static double lon2;
    static double EARTH_RADIUS = 3958.75; // in  miles

    public static double HaverSineDistance(double lat1, double lon1, double lat2, double lon2)
    {

        // Step 1: convert given latitudes and longitudes to radians
        lat1 = Math.toRadians(lat1);
        lon1 = Math.toRadians(lon1);
        lat2 = Math.toRadians(lat2);
        lon2 = Math.toRadians(lon2);

        // Step 2:
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;

        // Step 3:
        double a = Math.pow((Math.sin(dlat/2)),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);

        // Step 4:
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        // Return great circle distance between the two locations (sets of coordinates)
        return EARTH_RADIUS * c;
    }

    /**
     * Because the earth's geometry is more nearly ellipsoidal than spherical, there is
     * room for error in calculations that depend on the Haversine formula for
     * calculating great circle distance; up to 0.3% (although this typically matters at great distances
     * or if the application is relatively close to the poles!) The best alternative is the Vincenty formula
     * for calculating geodesic distance, but it is somewhat more conceptually demanding. The definitive
     * source is:
     *
     *      http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
     *
     * For now, I may use an existing Java implementation. Although, it is important to note
     * that Google Maps (Android) provides methods to perform geodesic calculations that are more than
     * likely Haversine variants (a geodesic is the shortest distance between two points on a sphere
     * or other curved surface). I haven't been able to verify this, but the language the authors use
     * in the developer's guide for the Geometry library in the JavaScript API suggest it is so:
     *
     *      https://developers.google.com/maps/documentation/javascript/geometry?hl=en
     *
     * I don't know if, and haven't been able to verify that, the same implementation is used in both
     * the JavaScript and Android APIs but I think it is safe to assume that they are!
     *
     * For the purposes of my application, it may not be necessary to have very great accuracy. All that
     * matters is that the application chooses the best match from a database using the relative
     * size of the geodesic that obtains between the coordinates Google gives for the nearest bus stop,
     * and the coordinates of that subset of records in the Dublinked database that contains the
     * string given by Google as the fullname of the bus stop of interest. For example, if Google's
     * Places/nearbysearch returns the coordinates for a bus stop in Parnell street, and the fullname
     * of the bus stop contains the string 'Parnell', then when the application queries the local
     * copy of the Dublinked bus stop information database for all records whose fullname field
     * contains 'Parnell', then the application can make a best guess at which of the records returned
     * has the real time information of interest, by assuming that the shortest geodesic, or the shortest
     * of the shortest curved surface distances uniquely identifies the record of interest! The distances
     * between the device and the various nearby bus stops will all be more or less equally inaccurate at the
     * scale of interest (within about 5km), and all that matters is that the smallest distance is chosen.
     * In fact, it is more than likely that my Vincenty algorithm may not be any more accurate than my
     * Haversine algorithm for choosing stop ids!
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public static double VincentyDistance(double lat1, double lon1, double lat2, double lon2)
    {
        double result = 0.0;
        // Put algorithm here!  TODO Vincenty Algorithm
        return result;
    }

}

