/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.system.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * This class exposes methods for accessing system services associated with network access.
 */
public class NetTools {

    /**
     * Uses <code>ConnectivityManager</code> to determine whether a network connection is available.
     * <code>ConnectivityManager</code> cannot be accessed from a static context, so it is necessary
     * to call the method (<code>getSystemService()</code>) from an instance of the abstract class
     * <code>Context</code>. Note that since an Activity indirectly subclasses <code>Context</code>
     * a given <code>Activity</code> may pass a reference to itself, in the BlackOut app MapsActivity,
     * <code>MapsActivity.this</code> may be passed to this method as a parameter.
     *
     * @return boolean (connected/not connected)
     * @param c instance of a class that subclasses Context
     */
    public static boolean checkConnectivity(Context c)
    {

        boolean connected;
        ConnectivityManager connMgr = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
        if(netInfo != null)
        {
            connected = true;
        }
        else
        {
            connected = false;
        }
        return connected;
    }
}
