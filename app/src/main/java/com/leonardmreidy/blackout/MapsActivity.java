/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout;

import android.app.DialogFragment;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.leonardmreidy.blackout.db.DBHelper;
import com.leonardmreidy.blackout.dialogs.FlightModeSettingsWarningDialogFragment;
import com.leonardmreidy.blackout.dialogs.WiFiDisabledWarningDialogFragment;

import java.sql.SQLException;

/**
 * TODO: JavaDoc for MapsActivity class
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private final String TAG = "## DEBUG/MAPS: ##";
    private boolean isConnected;
    private Toast connectionToast;
    private String startingCoordinates;
    private double startingLat;
    private double startingLng;
    private Marker startingMarker;
    private boolean isWiFiEnabled;
    private GoogleMap mMap;
    private DBHelper dublinkedDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, " onCreate() executing...");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        initDeviceServices();

        initDB();



        // Get bundle provided by splashscreen activity
        Bundle bundle = getIntent().getExtras();

        // Get starting coordinates from bundle
        startingCoordinates = bundle.getString("startingCoordinates");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        // Create custom floating action button (fab)
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        // Show fab
        fab.hide(false);

        // Animate fab
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fab.show(true);
                fab.setShowAnimation(AnimationUtils.loadAnimation(MapsActivity.this, R.anim.show_from_bottom));
                fab.setHideAnimation(AnimationUtils.loadAnimation(MapsActivity.this, R.anim.hide_to_bottom));
            }
        }, 300);

        // action listener for floating action button
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something
                startingMarker.setSnippet("FAB says hi!");
                startingMarker.showInfoWindow();
            }
        });
    }

    /**
     * Check if wifi is enabled and whether or not the device is in flight mode.
     * If wifi is not enabled, or the device is in flight mode, or both, notify the user
     * and present the appropriate settings activities so that the user can quickly fix
     * the problem.
     */
    public void initDeviceServices() {

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        boolean wiFiIsEnabled = wifi.isWifiEnabled();
        boolean flightModeIsEnabled = Settings.System.getInt(
                getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) == 1;

        if (flightModeIsEnabled) {
            DialogFragment flightModeFragment = new FlightModeSettingsWarningDialogFragment();
            flightModeFragment.show(getFragmentManager(), "Flight Mode is Enabled");
        }

        if (!wiFiIsEnabled) {
            DialogFragment wifiDisabledWarningDialogFragment = new WiFiDisabledWarningDialogFragment();
            wifiDisabledWarningDialogFragment.show(getFragmentManager(), "Wifi Not Available");
        }

    }

    public void initDB()
    {

        dublinkedDBHelper = new DBHelper(this,
                dublinkedDBHelper.getDatabaseName(),
                null,
                dublinkedDBHelper.getDatabaseVersion());
    }

    /**
     * Returns a String containing the relevant Dublinked Stop ID.
     * Given an instance of DBHelper, a set of coordinates, and a String to
     * compare to the fullname field in the database, get the Dublinked stop id
     * that is the closest match.
     * @param dublinkedDBHelper an instance of the DBHelper class
     * @param googleCoords a String representing the coordinates given by Google for the device
     * @param fullnameLike a String representing the fullname field in both data sources
     * @return the most likely Dublinked stop ID given the inputs
     */
    protected String getDublinkedStopId(DBHelper dublinkedDBHelper,
                                        String googleCoords,
                                        String fullnameLike)
    {
        String stopId = null;

        try {
            stopId = dublinkedDBHelper
                    .getMostLikelyStopIdFromQueryResults(dublinkedDBHelper
                                    .getQueryResultsFromCursor(dublinkedDBHelper
                                            .getRecord("%"+fullnameLike+"%")),
                            googleCoords);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return stopId;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we add the marker to the current location provided by the splash screen activity.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.i(TAG, " onMapReady() executing...");

        // Try to get starting location identified by splash screen activity,
        // if this is not available, fail as gracefully as possible
        try {

            // Parse Bundle extras for coordinates of starting location
            startingLat = Double.parseDouble(startingCoordinates.split(",")[0]);
            startingLng = Double.parseDouble(startingCoordinates.split(",")[1]);

        } catch (NumberFormatException nfe)
        {
            /**
             *  TODO: replace with dialog/settings activity
             *  For a useful discussion of this problem and solution see:
             *  http://stackoverflow.com/questions/32759530/how-to-update-the-android-app-when-the-location-toggle-is-enabled-in-settings
             *  The code below represents a temporary solution to a problem typically caused by
             *  disabling location services. It allows the application to fail 'gracefully' and
             *  notify the user that there is a problem
             *  with the location services.
             */

            startingLat = 53.352052;
            startingLng = -6.263137;
            Toast.makeText(this, "Please make sure location services are enabled!", Toast.LENGTH_LONG).show();

        }

        mMap = googleMap;

        // Set map padding to prevent floating action button overlapping map controls
        mMap.setPadding(20, 0, 130, 15);

        // Add marker at current location (retrieved from location provider while splash screen loaded)
        LatLng startingLatLng = new LatLng(startingLat, startingLng);
        startingMarker = mMap
                .addMarker(new MarkerOptions()
                        .position(startingLatLng)
                                //.title("Connected: " + isConnected));
                        .title("You are here mate!"));


        // Define a camera position
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(startingLatLng)
                .zoom(17)
                .bearing(90)
                .tilt(30)
                .build();

        // Zoom in to the current location with animation
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    @Override
    public void onMapLoaded() {

        //TODO: implement onMapLoaded()
    }
}
