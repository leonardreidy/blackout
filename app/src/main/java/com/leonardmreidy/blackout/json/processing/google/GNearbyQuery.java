/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.json.processing.google;

import android.content.Context;

/**
 * TODO: JavaDoc for GNearbyQuery
 */
public class GNearbyQuery
{
    private Context context;
    private String _XML = null;
    private String _JSON = null;

    private String resultFormat;
    private String types;
    private double qLat;        // (device) latitude for Places API query
    private double qLong;       // (device) longitude for Places API query
    private String rankBy;      // selection for ranking of Google API responses (by distance, location, etc)
    private String apiKey;
    private String query;

    // Default zero-arg constructor
    public GNearbyQuery()
    {
        this._XML = "xml";
        this._JSON = "json";
        this.resultFormat = _JSON;
        this.types = "transit_station";
        this.rankBy = "=distance";
        this.apiKey = "AIzaSyA6LwyzlFhJzsg0aGkbGCmWw5eo4TykZ2M";
    }

    // Development-friendly constructor
    public GNearbyQuery(double qLat, double qLong)
    {
        this(); // call given no-arg constructor
        this.qLat = qLat;
        this.qLong = qLong;

        setQuery(resultFormat, types, qLat, qLong, rankBy, apiKey);
    }

    // Default multi-arg constructor
    public GNearbyQuery(String types, double qLat, double qLong, String rankBy, String apiKey)
    {
        setResultFormat(_JSON);     // set default result format
        setTypes(types);
        setqLat(qLat);
        setqLong(qLong);
        setRankBy(rankBy);
        setApiKey(apiKey);

        setQuery(resultFormat, types, qLat, qLong, rankBy, apiKey);
    }

    /**
     * This method sets up a standard nearby query to the Google Places
     * API Web Service. This is typically for use in a server
     * application, but I am using it here temporarily until I have
     * written a simple RESTful web service for my application.
     * A NearbyUtils object exploits the Nearby Search request
     * query request to search for places within a specified area. An
     * example of the form of the request urls is below:
     *
     *      https://maps.googleapis.com/maps/api/place/nearbysearch/
     *      json?
     *      &radius=500
     *      &key=AIzaSyA6LwyzlFhJzsg0aGkbGCmWw5eo4TykZ2M
     *
     * @param resultFormat  resultFormat specifies the data output in JSON
     * @param types         types of nearby places (e.g., bus station)
     * @param qLat          device current latitude
     * @param qLong         device current longitude
     * @param rankBy        rank responses by distance, location, etc
     * @param apiKey        Google Maps API key
     *
     */
    public void setQuery(String resultFormat, String types,
                         double qLat, double qLong, String rankBy, String apiKey)
    {
        query = "https://maps.googleapis.com/maps/api/place/nearbysearch/"
                +resultFormat+"?"
                +"&types="+types
                +"&location="+qLat+","+qLong
                +"&rankby"+rankBy
                +"&key="+apiKey;
    }
    public String getQuery() {
        return query;
    }

    public String getResultFormat() {
        return resultFormat;
    }

    public void setResultFormat(String resultFormat) {
        this.resultFormat = resultFormat;
    }

    public double getqLat() {
        return qLat;
    }

    public void setqLat(double qLat) {
        this.qLat = qLat;
    }

    public double getqLong() {
        return qLong;
    }

    public void setqLong(double qLong) {
        this.qLong = qLong;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getRankBy()
    {
        return rankBy;
    }

    public void setRankBy(String rankBy)
    {
        this.rankBy = rankBy;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}

