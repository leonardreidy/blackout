/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.json.processing.dublinked;

/**
 * TODO: JavaDoc for DResult class
 */
public class DResult
{

    // Arrival time in dd/MM/yyyy HH:mm:ss format
    private String arrivaldatetime;

    // Arrival due time in minutes
    private String duetime;

    // Departure time in dd/MM/yyyy HH:mm:ss format
    private String departuredatetime;

    // Departure due time in minutes
    private String departureduetime;

    // Scheduled arrival time in dd/MM/yyyy HH:mm:ss format
    private String scheduledarrivaldatetime;

    // Scheduled departure time in dd/MM/yyyy HH:mm:ss format
    private String scheduleddeparturedatetime;

    // Service destination name
    private String destination;

    // Localised service destination name
    private String destinationlocalized;

    // Service origin name
    private String origin;

    // Localised service origin name
    private String originlocalized;

    // Service direction
    private String direction;

    // Bus service operator name
    private String operator;

    // Additional information
    private String additionalinformation;

    // Bus low floor status (disabled ramp) [Yes or No]
    private String lowfloorstatus;

    // Route
    private String route;

    // Timestamp in dd/MM/yyyy HH:mm:ss format
    private String sourcetimestamp;

    // Monitored isn't documented in the RTPI REST API spec
    private String monitored;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getArrivaldatetime() {
        return arrivaldatetime;
    }

    public void setArrivaldatetime(String arrivaldatetime) {
        this.arrivaldatetime = arrivaldatetime;
    }

    public String getDuetime() {
        return duetime;
    }

    public void setDuetime(String duetime) {
        this.duetime = duetime;
    }

    public String getDeparturedatetime() {
        return departuredatetime;
    }

    public void setDeparturedatetime(String departuredatetime) {
        this.departuredatetime = departuredatetime;
    }

    public String getDepartureduetime() {
        return departureduetime;
    }

    public void setDepartureduetime(String departureduetime) {
        this.departureduetime = departureduetime;
    }

    public String getScheduledarrivaldatetime() {
        return scheduledarrivaldatetime;
    }

    public void setScheduledarrivaldatetime(String scheduledarrivaldatetime) {
        this.scheduledarrivaldatetime = scheduledarrivaldatetime;
    }

    public String getScheduleddeparturedatetime() {
        return scheduleddeparturedatetime;
    }

    public void setScheduleddeparturedatetime(String scheduleddeparturedatetime) {
        this.scheduleddeparturedatetime = scheduleddeparturedatetime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationlocalized() {
        return destinationlocalized;
    }

    public void setDestinationlocalized(String destinationlocalized) {
        this.destinationlocalized = destinationlocalized;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginlocalized() {
        return originlocalized;
    }

    public void setOriginlocalized(String originlocalized) {
        this.originlocalized = originlocalized;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAdditionalinformation() {
        return additionalinformation;
    }

    public void setAdditionalinformation(String additionalinformation) {
        this.additionalinformation = additionalinformation;
    }

    public String getLowfloorstatus() {
        return lowfloorstatus;
    }

    public void setLowfloorstatus(String lowfloorstatus) {
        this.lowfloorstatus = lowfloorstatus;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getSourcetimestamp() {
        return sourcetimestamp;
    }

    public void setSourcetimestamp(String sourcetimestamp) {
        this.sourcetimestamp = sourcetimestamp;
    }

    public String getMonitored() {
        return monitored;
    }

    public void setMonitored(String monitored) {
        this.monitored = monitored;
    }



}
