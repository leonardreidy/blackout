/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.json.processing.dublinked;
import java.util.List;

public class DRoot
{

    /**Dublinked RTPI REST API/Response error codes
     *
     *      0   Success
     *      1   No results
     *      2   Missing parameter
     *      3   Invalid parameter
     *      4   Scheduled downtime
     *      5   Unexpected system error
     *
     */
    private String errorcode;

    // Additional error details
    private String errormessage;

    // Number of results
    private int numberofresults;

    // Stop id
    private int stopid;

    // Date/time information returned
    private String timestamp;

    /**
     * An array of 17 key/value pairs representing the
     * principal target information to be extracted
     * and used by the application. See the comments
     * associated with the Result class member variables.
     */
    private List<DResult> results;

    public String getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public int getNumberofresults() {
        return numberofresults;
    }

    public void setNumberofresults(int numberofresults) {
        this.numberofresults = numberofresults;
    }

    public int getStopid() {
        return stopid;
    }

    public void setStopid(int stopid) {
        this.stopid = stopid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<DResult> getResults() {
        return results;
    }

    public void setResults(List<DResult> results) {
        this.results = results;
    }



}

