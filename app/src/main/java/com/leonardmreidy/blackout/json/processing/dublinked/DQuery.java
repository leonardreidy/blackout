/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.json.processing.dublinked;

import android.util.Log;

/**
 * A simple class for building Dublinked query urls. The Dublinked RTPI REST web service
 * exposes methods to:
 *
 *      Retrieve Real Time Bus Information
 *      Retrieve Timetable Bus Information by Date
 *      Retrieve Full Timetable Bus Information
 *      Retrieve Bus Stop Information
 *      Retrieve Route Information
 *      Retrieve Operator Information
 *      Retrieve Route List Information
 *
 * A standard Dublinked query for a given stop id looks like this:
 *
 *      http://www.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?stopid=7602&format=json
 *
 * The general URI pattern for this request type is:
 *
 *      http://[rtpiserver]/busstopinformation?stopid=[stopid]&stopname=[stopname]&format=[format]
 */
public class DQuery
{
    private String baseUrl;
    private String stopIdParam;
    private String stopId;
    private String formatParam;
    private String format;
    private String query;


    // Simple default constructor for testing purposes
    public DQuery()
    {
        baseUrl = "http://www.dublinked.ie/cgi-bin/rtpi/realtimebusinformation";
        stopIdParam = "?stopid=";
        stopId = "7602"; // for testing
        formatParam = "&format=";
        format = "json";
        setQuery(baseUrl+stopIdParam+stopId+formatParam+format);
        Log.i("DublinkedUtils Const", "## DEBUG ##: DublinkedUtils zero-arg constructor executing...+\n Query = "+getQuery());
    }

    public DQuery(String stopId)
    {
        baseUrl = "http://www.dublinked.ie/cgi-bin/rtpi/realtimebusinformation";
        stopIdParam = "?stopid=";
        formatParam = "&format=";
        format = "json";
        setQuery(baseUrl+stopIdParam+stopId+formatParam+format);
        Log.i("DublinkedUtils Const", "## DEBUG ##: DublinkedUtils one-arg constructor executing...+\n Query = " + getQuery());
    }

    public void setQuery(String queryUrl)
    {
        query = queryUrl;
    }

    public String getQuery()
    {
        return query;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getStopIdParam() {
        return stopIdParam;
    }

    public void setStopIdParam(String stopIdParam) {
        this.stopIdParam = stopIdParam;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getFormatParam() {
        return formatParam;
    }

    public void setFormatParam(String formatParam) {
        this.formatParam = formatParam;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

}
