/**
 * Copyleft (C) 2015 Leonard M Reidy
 *
 * Not licensed under the Apache License, Version 2.0 (the 'License'), or indeed, any other license.
 * This copyleft statement is essentially a placeholder designed to fulfill Android coding style
 * conventions. According to the 'Code Style for Contributors' guide at:
 *
 *  https://source.android.com/source/code-style.html#java-library-rules
 *
 *  Android follows standard Java coding conventions, with some additional rules including:
 *
 *  Every file should have a copyright statement at the top
 *  Don't Ignore Exceptions
 *  Don't Catch Generic Exception
 *  Don't Use Finalizers
 *  Fully Qualify Imports
 *  Use Javadoc Standard Comments
 *  Write Short Methods (< 40 lines)
 *  Define Fields in Standard Places
 *  Limit Variable Scope
 *  Order Import Statements (android imports, com, junit, net, org..., java and javax)
 *  Use Spaces For Indentation (I choose to ignore this for now)
 *  Follow Field Naming Conventions
 *  Use Standard Brace Style
 *  Limit Line Length (< 100 characters long)
 *  Use Standard Java Annotations
 *  Treat Acronyms as Words
 *  Use 'TODO' comments
 *  Log sparingly
 *
 *  Finally, this project attempts to respect Semantic Versioning 2.0.0 for application versioning:
 *  http://semver.org/
 *
 *  @author Leonard M Reidy
 *  @version 0.0.1
 */
package com.leonardmreidy.blackout.json.processing.google;
import android.util.Log;

/**
 * TODO: JavaDoc for GDirectionQuery
 * TODO: Consider collapsing all Query classes into a single class
 */
public class GDirectionQuery
{
    public final String _XML = "xml";
    public final String _JSON = "json";
    private String query;
    private String resultFormat;
    private double qLat;
    private double qLng;
    private double destLat;
    private double destLng;
    private String mode;
    private String apiKey;
    private String destination;

    public GDirectionQuery()
    {
        // TODO implement default zero-arg constructor for DirectionUtils
    }

    // Default multi-arg constructor
    public GDirectionQuery(double qLat, double qLong, String apiKey)
    {
        setResultFormat(_JSON);  // set default result format
        setQLat(qLat);
        setQLng(qLng);
        setDestLat(destLat);
        setDestLng(destLng);
        setDestination(destLat, destLng);
        setMode(mode);
        setApiKey(apiKey);

        setQuery(resultFormat, qLat, qLong, destLat, destLng, mode, apiKey);
    }

    public void setQuery(String resultFormat, double qLat, double qLong,
                         double destLat, double destLng, String mode, String apiKey)
    {
        query = "https://maps.googleapis.com/maps/api/directions/"
                +resultFormat
                +"?origin="+qLat+","+qLong
                +"&destination="+destLat+","+destLng
                +"&mode="+mode
                +"&key="+apiKey;
        Log.i(query, "DirectionUtils.setQuery() returns: ");
    }

    public String getResultFormat() {
        return resultFormat;
    }

    public void setResultFormat(String resultFormat) {
        this.resultFormat = resultFormat;
    }

    public double getQLat() {
        return qLat;
    }

    public void setQLat(double qLat) {
        this.qLat = qLat;
    }

    public double getQLng() {
        return qLng;
    }

    public void setQLng(double qLng) {
        this.qLng = qLng;
    }

    public double getDestLat() {
        return destLat;
    }

    public void setDestLat(double destLat) {
        this.destLat = destLat;
    }

    public double getDestLng() {
        return destLng;
    }

    public void setDestLng(double destLng) {
        this.destLng = destLng;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setDestination(double destLat, double destLng)
    {
        this.destLat = destLat;
        this.destLng = destLng;
    }

    public String getDestination()
    {
        return destLat+","+destLng;
    }


    public String getQuery()
    {
        return query;
    }
}

